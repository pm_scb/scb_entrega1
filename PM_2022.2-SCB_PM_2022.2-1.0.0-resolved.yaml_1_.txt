---
openapi: 3.0.0
info:
  title: Simple Inventory API
  description: This is a simple API
  contact:
    email: you@your-company.com
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
- url: https://virtserver.swaggerhub.com/PM_2022.2/SCB_PM_2022.2/1.0.0
  description: SwaggerHub API Auto Mocking
- url: https://virtserver.swaggerhub.com/hvictorleite/PM_SCB/1.0.0
  description: SwaggerHub API Auto Mocking
tags:
- name: Ciclista
- name: Bicicleta
- name: Funcionario
- name: Tranca
- name: Totem
paths:
  /ciclista:
    post:
      tags:
      - Ciclista
      summary: Cadastrar ciclista
      description: |
        Cadastrar um ciclista no sistema
      operationId: cadastrarCiclista
      requestBody:
        description: Ciclista a ser cadastrado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Ciclista'
      responses:
        "201":
          description: Ciclista cadastrado com sucesso.
        "400":
          description: Dados inválidos
  /bicicleta:
    get:
      tags:
      - Bicicleta
      summary: Listar bicicletas
      description: |
        Lista todas as bicicletas cadastradas no sistema
      operationId: getBicicletas
      responses:
        "200":
          description: Resultados da busca com sucesso de todas as bicicletas do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Bicicleta'
        "404":
          description: Não foram encontradas bicicletas no sistema
    post:
      tags:
      - Bicicleta
      summary: Cadastrar bicicleta
      requestBody:
        description: Bicicleta a ser cadastrada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Bicicleta'
      responses:
        "201":
          description: Bicicleta cadastrada com sucesso.
  /bicicleta/{numero}:
    put:
      tags:
      - Bicicleta
      summary: Atualizar uma bicicleta existente
      description: |
        Atualiza uma bicicleta do sistema mediante o identificador único.
      operationId: atualizarBicicleta
      parameters:
      - name: numero
        in: path
        description: Número da bicicleta a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Bicicleta a ser atualizada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Bicicleta'
      responses:
        "200":
          description: Bicicleta atualizada com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Bicicleta não encontrada
    delete:
      tags:
      - Bicicleta
      summary: Remover uma bicicleta existente
      description: |
        Remove uma bicicleta do sistema mediante o identificador único.
      operationId: removerBicicleta
      parameters:
      - name: numero
        in: path
        description: Número da bicicleta a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Bicicleta removida com sucesso
        "404":
          description: Bicicleta não encontrada
  /funcionario:
    get:
      tags:
      - Funcionario
      summary: Listar funcionários
      description: |
        Lista todos os funcionários cadastrados no sistema
      operationId: getFuncionarios
      responses:
        "200":
          description: Resultados da busca com sucesso de todos os funcionários do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Funcionario'
        "404":
          description: Não foram encontrados funcionários no sistema
    post:
      tags:
      - Funcionario
      summary: Cadastrar funcionário
      requestBody:
        description: Funcionário a ser cadastrado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Funcionario'
      responses:
        "201":
          description: Funcionário cadastrado com sucesso.
  /funcionario/{matricula}:
    put:
      tags:
      - Funcionario
      summary: Atualizar um funcionário existente
      description: |
        Atualiza uma funcionário cadastrado no sistema
      operationId: atualizarFuncionario
      parameters:
      - name: matricula
        in: path
        description: Número da matrícula do funcionário a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Funcionario a ser atualizado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Funcionario'
      responses:
        "200":
          description: Funcionario atualizado com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Funcionario não encontrado
    delete:
      tags:
      - Funcionario
      summary: Remover uma funcionário existente
      description: |
        Remove um funcionário do sistema mediante o identificador único.
      operationId: removerFuncionario
      parameters:
      - name: matricula
        in: path
        description: Matrícula do funcionário a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Funcionário removido com sucesso
        "404":
          description: Funcionário não encontrado
  /tranca:
    get:
      tags:
      - Tranca
      summary: Listar trancas
      description: |
        Lista todas as trancas cadastradas no sistema
      operationId: getTrancas
      responses:
        "200":
          description: Resultados da busca com sucesso de todas as trancas do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Tranca'
        "404":
          description: Não foram encontradas trancas no sistema
    post:
      tags:
      - Tranca
      summary: Cadastrar tranca
      requestBody:
        description: Tranca a ser cadastrada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tranca'
      responses:
        "201":
          description: Tranca cadastrada com sucesso.
  /tranca/{numero}:
    put:
      tags:
      - Tranca
      summary: Atualizar uma tranca existente
      description: |
        Atualiza uma tranca cadastrada no sistema
      operationId: atualizarTranca
      parameters:
      - name: numero
        in: path
        description: Número da tranca a atualizar
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      requestBody:
        description: Tranca a ser atualizada.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Tranca'
      responses:
        "200":
          description: Tranca atualizada com sucesso
        "400":
          description: Dados inválidos
        "404":
          description: Tranca não encontrada
    delete:
      tags:
      - Tranca
      summary: Remover uma tranca existente
      description: |
        Remove uma tranca do sistema mediante o identificador único.
      operationId: removerTranca
      parameters:
      - name: numero
        in: path
        description: Número da tranca a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Tranca removida com sucesso
        "404":
          description: Tranca não encontrada
  /totem:
    get:
      tags:
      - Totem
      summary: Listar totens
      description: |
        Lista todos os totens cadastrados no sistema
      operationId: getTotens
      responses:
        "200":
          description: Resultados da busca com sucesso de todos os totens do sistema
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Totem'
        "404":
          description: Não foram encontrados totens no sistema
    post:
      tags:
      - Totem
      summary: Cadastrar totem
      requestBody:
        description: Totem a ser cadastrado.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Totem'
      responses:
        "201":
          description: Totem cadastrado com sucesso.
  /totem/{numero}:
    delete:
      tags:
      - Totem
      summary: Remover uma totem existente
      description: |
        Remove um totem do sistema mediante o identificador único.
      operationId: removerTotem
      parameters:
      - name: numero
        in: path
        description: Número do totem a remover
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int32
      responses:
        "204":
          description: Totem removido com sucesso
        "404":
          description: Totem não encontrado
components:
  schemas:
    Ciclista:
      required:
      - cartaoCredito
      - cpf
      - email
      - fotoDocumento
      - indicadorBrasileiroOuEstrangeiro
      - nome
      - passaporte
      - senha
      type: object
      properties:
        email:
          type: string
          format: Email
          example: joaocandido@email.com
        nome:
          type: string
          example: João Cândido
        indicadorBrasileiroOuEstrangeiro:
          type: boolean
        cpf:
          type: string
          example: 000.000.000-00
        passaporte:
          $ref: '#/components/schemas/Passaporte'
        senha:
          type: string
          format: password
          example: $2a$11$MXOOO1JYngri2arcL6Cic.KuBujhqgz.B2ri6szqN2/cfsdiQa7se
        cartaoCredito:
          $ref: '#/components/schemas/CartaoCredito'
        fotoDocumento:
          type: string
          format: url
          example: https://www.aws.com/image
    Passaporte:
      required:
      - dataValidade
      - pais
      - passaporte
      type: object
      properties:
        passaporte:
          type: string
          example: CS265436
        dataValidade:
          type: string
          format: date
          example: 2016-08-29
        pais:
          type: string
          example: Brazil
    CartaoCredito:
      required:
      - codigoSeguranca
      - nome
      - numero
      - validade
      type: object
      properties:
        numero:
          type: string
          example: 4444-4444-4444-4444
        nome:
          type: string
          example: João Cândido
        validade:
          type: string
          format: date
        codigoSeguranca:
          type: string
          example: "123"
    Bicicleta:
      required:
      - ano
      - marca
      - modelo
      - numero
      - status
      type: object
      properties:
        numero:
          type: integer
          example: 1
        marca:
          type: string
          example: Hondiz
        modelo:
          type: string
          example: Urban
        ano:
          type: string
          format: year
          example: YYYY
        status:
          type: string
          format: enum
          example: Nova
    Funcionario:
      required:
      - cpf
      - funcao
      - idade
      - matricula
      - nome
      - senha
      type: object
      properties:
        matricula:
          type: integer
          example: 1
        senha:
          type: string
          format: password
          example: $2a$11$MXOOO1JYngri2arcL6Cic.KuBujhqgz.B2ri6szqN2/cfsdiQa7se
        nome:
          type: string
          example: João Cândido
        idade:
          type: integer
          example: 40
        funcao:
          type: string
          example: reparador
        cpf:
          type: string
          example: 000.000.000-00
    Tranca:
      required:
      - anoFabricacao
      - modelo
      - numero
      - status
      type: object
      properties:
        numero:
          type: integer
          example: 1
        status:
          type: string
          format: enum
          example: Nova
        anoFabricacao:
          type: string
          format: year
          example: YYYY
        modelo:
          type: string
          example: SuperBlock
    Totem:
      required:
      - descricao
      - numero
      type: object
      properties:
        numero:
          type: integer
          example: 1
        descricao:
          type: string
